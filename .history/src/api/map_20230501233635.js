import request2 from '@/utils/request2'
export const guideRoute = (address) => {
  return request2({
    url: '/v3/geocode/geo',
    method: 'GET'
  })
}
