import request from '@/utils/request'
// 获取所有酒店
export const getHotelList = () => {
  return request({
    method: 'GET',
    url: '/api/hotel'
  })
}
// 通过id查询酒店信息
export const getHotelInfo = (id) => {
  return request({
    method: 'GET',
    url: `/api/v1/hotel/${id}`
  })
}
export const getAllRoomInfoByHotlId = (id) => {
  return request({
    method: 'GET',
    url: `/room/ByHotelId/${id}`
  })
}
// 按照酒店价格升序排列
export const getHotelSort = () => {
  return request({
    method: 'GET',
    url: '/hotel/sortPrice'
  })
}
