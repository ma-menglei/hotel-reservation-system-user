import request from '@/utils/request'
// 根据用户id获取订单
export const getOrderinfo = (id) => {
  return request({
    url: `/user/reservation/${id}`,
    method: 'GET'
  })
}
// 根据订单id删除订单
export const delOrder = (orderID) => {
  return request({
    url: `/api/reservation/${orderID}`,
    method: 'DELEYE'
  })
}
