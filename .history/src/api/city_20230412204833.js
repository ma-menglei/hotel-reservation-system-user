import request from '@/utils/request'
// 获取城市列表
export const getCityList = () => {
  return request({
    method: 'GET',
    url: '/api/city'
  })
}
// 根据城市id查询器对应的所有酒店信息

export const getCityHotelList = (id) => {
  return request({
    method: 'GET',
    url: `/api/city/${id}`
  })
}
