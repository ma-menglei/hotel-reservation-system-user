import request from '@/utils/request'
// 根据用户id获取订单
export const getOrderinfo = (id) => {
  return request({
    url: `/user/reservation/${id}`
  })
}
