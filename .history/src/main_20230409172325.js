import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import '@/elementUI/index'
Vue.config.productionTip = false
// 创建全局地图对象
const map = new AMap.Map('mapContainer', {
  zoom: 13,
  center: [116.397428, 39.90923],
  viewMode: '3D'
})
new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
