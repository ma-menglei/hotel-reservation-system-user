import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import '@/elementUI/index'
// 导入格式化时间包
import dayjs from 'dayjs'
Vue.config.productionTip = false
// 定义格式化事件函数 格式化到秒
Vue.prototype.$fromatDate = (dateObj) => {
  return dayjs(dateObj).format('YYYY-MM')
}
Vue.prototype.$fromatDateDay = (dateObj) => {
  return dayjs(dateObj).format('DD')
}
Vue.prototype.$fromatDateAll = (dateObj) => {
  return dayjs(dateObj).format('YYYY-MM-DD hh:mm:ss')
}
new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
