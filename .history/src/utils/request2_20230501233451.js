import axios from 'axios'

// import router from '@/router'
// import { Message } from 'element-ui'
// import { reject } from 'core-js/fn/promise'
// import { config } from 'vue/types/umd'
export const baseURL = 'https://restapi.amap.com' // 接口和文档所在的地址
// 创建一个自定义的axios
// myAxios请求的时候地址=baseURL+url
const myAxios = axios.create({
  baseURL: baseURL
})
// console.log(myAxios)

// 导出自定义的axios方法，供外部调用发请求
export default myAxios
