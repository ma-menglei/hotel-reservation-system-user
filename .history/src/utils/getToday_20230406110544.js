export function getToday () {
  const date = new Date()

  const options = { month: '2-digit', day: '2-digit' }

  return date.toLocaleDateString('zh-CN', options).replace(/\//g, '月')
}

// 对表单中的时间进行格式化
export function formatDate (date) {
  const year = date.getFullYear()
  const month = addZero(date.getMonth() + 1)
  const day = addZero(date.getDate())
  const hour = addZero(date.getHours())
  const minute = addZero(date.getMinutes())
  const second = addZero(date.getSeconds())
  return `${year}-${month}-${day} ${hour}:${minute}:${second}`
}
function addZero (num) {
  return num < 10 ? `0${num}` : num
}
// 计算两个时间之间有几个晚上
