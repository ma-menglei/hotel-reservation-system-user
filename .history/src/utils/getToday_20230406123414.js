export function getToday () {
  const date = new Date()

  const options = { month: '2-digit', day: '2-digit' }

  return date.toLocaleDateString('zh-CN', options).replace(/\//g, '月')
}

// 对表单中的时间进行格式化
export function formatDate (date) {
  const year = date.getFullYear()
  const month = addZero(date.getMonth() + 1)
  const day = addZero(date.getDate())
  const hour = addZero(date.getHours())
  const minute = addZero(date.getMinutes())
  const second = addZero(date.getSeconds())
  return `${year}-${month}-${day} ${hour}:${minute}:${second}`
}
function addZero (num) {
  return num < 10 ? `0${num}` : num
}
// 计算两个时间之间有几个晚上
export function getNightCount (date1, dateTwo) {
  const diffTime = dateTwo.getTime() - date1.getTime()
  const diffDays = Math.ceil(diffTime / (1000 * 60 * 60 * 24))

  let nights = 0
  if (diffDays === 1 && date1.getHours() >= 18) {
    nights = 1
  } else if (diffDays > 1 || (diffDays === 1 && (date1.getHours() >= 18 || dateTwo.getHours() < 6))) {
    const start = new Date(date1)
    start.setHours(18, 0, 0, 0)
    const end = new Date(dateTwo)
    end.setHours(6, 0, 0, 0)
    const diffTime = end.getTime() - start.getTime()
    const diffDays = Math.ceil(diffTime / (1000 * 60 * 60 * 24))
    nights = diffDays
  }

  return nights
}
