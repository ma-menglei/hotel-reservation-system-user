import axios from 'axios'
import store from '@/store/index'
// import router from '@/router'
// import { Message } from 'element-ui'
// import { reject } from 'core-js/fn/promise'
// import { config } from 'vue/types/umd'
export const baseURL = 'http://127.0.0.1:7001' // 接口和文档所在的地址
// 创建一个自定义的axios
// myAxios请求的时候地址=baseURL+url
const myAxios = axios.create({
  baseURL: baseURL
})
// console.log(myAxios)

// 定义请求拦截器
// axios里每次调用request都会先走这个请求拦截器
myAxios.interceptors.request.use(function (config) {
  // 在请求前触发一次 这个return 交给axios源码内,根据配置项发起请求
  // 发起请求时,统一携带请求头
  // 登录和注册时,vuex无token,其他页面有token
  if (store.state.token) {
    // 如果vuex理由token，就会把token配置到请求头上一块发给后台
    config.headers.Authorization = `Bearer ${store.state.token}`
    config.headers.token = store.state.token
  }
  // console.log(config)
  return config
}, function (error) {
  return Promise.reject(error)
})

// 定义响应拦截器
myAxios.interceptors.response.use(function (response) {
  // 响应http状态码为2xx或3xx时触发成功的回调,形参中response是成功的结果

  return response
}, function (error) {
  // 响应http状态码为4xx或5xx触发失败的回调.error是失败的结果
  if (error.response.status === 401) {
    // 本次token过期,此时到清除token,返回登录页面 清除vuex的所有东西
    // 手动让token过期就是破坏localstorage里面的值然后刷新页面
    store.commit('setToken', '')
    store.commit('updateUserInfo', {})
    this.$router.push('/userLogin')
    this.$message.error('用户身份已过期')
  }
  return Promise.reject(error)
})

// 导出自定义的axios方法，供外部调用发请求
export default myAxios
