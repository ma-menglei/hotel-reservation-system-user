function getTomorrow () {
  const today = new Date()
  const tomorrow = new Date(today)
  tomorrow.setDate(today.getDate() + 1)
  const options = { month: '2-digit', day: '2-digit' }

  return tomorrow.toLocaleDateString('zh-CN', options).replace(/\//g, '月')
}
console.log(getTomorrow())
