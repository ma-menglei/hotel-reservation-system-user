import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import '@/elementUI/index'
import AMapLoader from '@amap/amap-jsapi-loader'
Vue.config.productionTip = false
// 创建全局地图对象
AMapLoader.load({
  key: '09a3b5266284dd13cda2392545575657', // 设置您的key
  version: '2.0',
  plugins: ['AMap.ToolBar', 'AMap.Driving']

}).then(AMap => {

})
new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
