export default {
  namespaced: true, // 开启命名空间
  state: {
    hotel: {}
  },
  mutations: {
    setHotel (state, newVal) {
      state.hotel = newVal
    }
  },
  actions: {},
  getters: {}
}
