import Vue from 'vue'
import Vuex from 'vuex'
import createPersistedState from 'vuex-persistedstate'
import hotel from './hotel'
import { getItem, setItem } from '@/utils/storage.js'
Vue.use(Vuex)
const TOKEN_KEY = 'HOTEL_USER'
export default new Vuex.Store({
  state: {
    token: getItem('TOKEN_KEY'), // 存储token
    userinfo: {} // 存储用户信息
  },
  getters: {
    nickname: state => state.userinfo.nickname,
    userinfo: state => state.userinfo
  },
  mutations: {

    setToken (state, data) {
      state.token = data
      setItem(TOKEN_KEY, state.token)
    },
    updateUserInfo (state, info) {
      state.userinfo = info
    }
  },
  actions: {
  },
  modules: {
    hotel
  },
  plugins: [
    createPersistedState() // 注入持久化插件
  ]
})
