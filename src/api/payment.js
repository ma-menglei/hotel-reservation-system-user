import request from '@/utils/request'
// 创建预约订单
export const addReserList = (data) => {
  return request({
    url: '/api/reservation',
    method: 'POST',
    data
  })
}
// 付款支付宝连接
export const goPayment = (data) => {
  return request({
    url: '/api/payment',
    method: 'POST',
    data
  })
}

// 查询交易状态
export const queryPayState = (orderID) => {
  return request({
    url: `/api/payment/queryOrderAlipay?orderID=${orderID}`,
    method: 'GET'

  })
}
