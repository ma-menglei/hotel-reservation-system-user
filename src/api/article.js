import request from '@/utils/request'
// 获取文章列表
export const getArticle = () => {
  return request({
    url: '/api/article',
    method: 'GET'
  }
  )
}
// 根据id获取文章信息
export const getArticleDetail = (id) => {
  return request({
    url: `/api/article/${id}`,
    method: 'GET'
  }
  )
}
// 获取所有留言信息
export const getComment = ({ page, pageSize }) => {
  return request({
    url: `/hotel/comment?page=${page}&pageSize=${pageSize}`,
    method: 'GET'
  })
}
// 新增留言
export const addComment = (data) => {
  return request({
    url: '/hotel/comment',
    method: 'POST',
    data
  })
}
