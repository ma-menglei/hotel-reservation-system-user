import request2 from '@/utils/request2'
export const guideRoute = (address, key) => {
  return request2({
    url: `/v3/geocode/geo?address=${address}&key=${key}`,
    method: 'GET'
  })
}
