// 封装具体的接口请求方法
// 注意：每个方法只负责请求一个url地址
import request from '@/utils/request'
// import store from '@/store/index'

// 导出接口方法
// registerAPI(this.form)
// 相当于 registerAPI({
//   username: '',
//   password: '',
//   repassword: ''
// })
// es6规定当key和value变量同名时，可以简写
// 注册的接口
export const registerAPI = ({ username, password, nickname }) => {
  // return是这个promise对象到逻辑页面，去那边对promise对象提取结果
  return request({
    url: '/user',
    method: 'POST',
    data: {
      username,
      password,
      nickname
    }
  })
}
// 管理员登陆的接口
export const adminloginAPI = ({ username, password }) => {
  return request({
    url: '/adminLogin',
    method: 'POST',
    data: {
      username,
      password
    }
  })
}
// http://127.0.0.1:7001/userLogin
// 用户登录
export const userloginAPI = ({ username, password }) => {
  return request({
    url: '/userLogin',
    method: 'POST',
    data: {
      username,
      password
    }
  })
}
// // 获取用户信息的接口
// export const getUserInfoAPI = () => {
//   return request({
//     url: '/my/userinfo',
//     method: 'GET'
//     // 传参给后台。params查询字符串，data请求体body，headers请求头
//     // headers: {
//     //   Authorization: store.state.token
//     // }
//   })
// }
