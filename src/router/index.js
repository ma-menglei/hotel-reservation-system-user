import Vue from 'vue'
import VueRouter from 'vue-router'
// import HomeView from '../views/HomeView.vue'
import store from '@/store/index'
Vue.use(VueRouter)
const originPush = VueRouter.prototype.push
const originReplace = VueRouter.prototype.replace
// 重写push
VueRouter.prototype.push = function (location, resolve, reject) {
  if (resolve && reject) {
    originPush.call(this, location, resolve, reject)
  } else {
    originPush.call(this, location, () => {}, () => {})
  }
}
// 重写replace
VueRouter.prototype.replace = function (location, resolve, reject) {
  if (resolve && reject) {
    originReplace.call(this, location, resolve, reject)
  } else {
    originReplace.call(this, location, () => {}, () => {})
  }
}
// 配置路由
const routes = [

  {
    path: '',
    redirect: '/home'
  },
  // 首页
  {
    path: '/home',
    component: () => import('@/views/Home/index'),
    meta: {
      show: true // 是否展示此组件
    }

  },
  {
    path: '/hotel-book',
    name: 'HotelBook',
    component: () => import('@/views/hotel-book/index'),
    meta: {
      show: true // 是否展示此组件
    }
  },
  {
    path: '/hotel-info',
    name: 'HotelInfo',
    component: () => import('@/views/hotel-book/hotel-info/index'),
    meta: {
      show: true // 是否展示此组件
    }
  },
  {
    path: '/user-message',
    name: 'UserMessage',
    component: () => import('@/views/user-message/index'),
    meta: {
      show: true // 是否展示此组件
    }
  },
  {
    path: '/hotel-news',
    name: 'HotelNews',
    component: () => import('@/views/hotel-news/index'),
    meta: {
      show: true // 是否展示此组件
    }
  },

  {
    path: '/article-info',
    name: 'ArticleInfo',
    component: () => import('@/views/hotel-news/article/article'),
    meta: {
      show: true // 是否展示此组件
    }
  },
  {
    path: '/order-info',
    name: 'OrderInfo',
    component: () => import('@/views/order-info/index.vue'),
    meta: {
      show: true // 是否展示此组件
    }
  },
  {
    path: '/my-order',
    name: 'MyOrder',
    component: () => import('@/views/my-order/myOrder.vue'),
    beforeRouteEnter (to, from, next) {
      // ...
      const token = this.$store.state.token
      const userinfo = this.$store.state.userinfo
      if (token && userinfo) {
        next()
      } else {
        this.$router.push('/userlogin')
      }
    },
    meta: {
      show: true // 是否展示此组件
    }

  },
  {
    path: '/about-us',
    name: 'AboutUs',
    component: () => import('@/views/about-us/AboutUs'),
    meta: {
      show: true // 是否展示此组件
    }
  },
  {
    path: '/search/:keyword',
    name: 'search',
    component: () => import('@/views/Search/index'),
    meta: {
      show: true
    }
  },
  // 用户注册
  {
    path: '/userReg',
    component: () => import('@/views/register/userReg'),
    meta: {
      show: false
    }
  },
  // 管理员注册
  {
    path: '/adminReg',
    component: () => import('@/views/register/adminReg'),
    meta: {
      show: false
    }
  },
  {
    // 用户登录
    path: '/userlogin',
    name: 'userLogin',
    component: () => import('@/views/login/userLogin'),
    meta: {
      show: false
    }
    // component: () => import('@/App')
  },
  {
    path: '/adminlogin',
    component: () => import('@/views/login/adminLogin'),
    meta: {
      show: false
    }
  },
  {
    path: '/search',
    component: () => import('@/views/Search/index'),
    meta: {
      show: false
    }
  }
]

const router = new VueRouter({
  routes
})
const whiteList = ['/order-info', '/my-order'] // 路由白名单
// 全局前置路由守卫
router.beforeEach((to, from, next) => {
  const token = store.state.token
  if (token) {
    next()
  } else {
    // 未登录,跳转到登陆界面
    if (whiteList.includes(to.path)) {
      // 数组.includes(值),作用:判断值是否在数组里出现过,出现过原地返回true  如果在白名单 则是要登录
      alert('您还没有登录，请先登录在进行操作')
      next('/userlogin')
    } else {
      // next强制切换到登录路径,会再次走了有==路由守卫再去匹配路由表
      // this.$router.push('/userlogin')
      next()
    }
  }
})
export default router
